import { LatchedSequence } from "blueshell";
import { Instance, log, PseudoState, PseudoStateKind, Region, State } from "@steelbreeze/state";
import { ActionLog } from "./ActionLog";
import { ActionWait } from "./ActionWait";
import { CharacterBlackboard } from "./CharacterBlackboard";
import { UpdateCharacterBehaviorEvent as UpdateCharacterBehaviorEvent } from "./UpdateCharacterBehaviorEvent";
import { UpdateCharacterMachineEvent } from "./UpdateCharacterMachineEvent";
import { Character } from "./Character";
import { CharacterState } from "./CharacterState";

let step = 0;

function update(): void {
  console.log(`UPDATE ${step}`);

  // behavior of current state
  (character.instance.get(character.region) as CharacterState).behavior.handleEvent(character.blackboard, new UpdateCharacterBehaviorEvent());
}

async function updateLoop() {
  while (true) {
    step++;

    update();

    // transitions state machine
    character.instance.evaluate(new UpdateCharacterMachineEvent(character));

    await new Promise(resolve => setInterval(resolve, 1000));
  }
}

// CREATE DEFINITION, BEHAVIOR

// state - grounded
const groundedBehavior0 = new ActionLog("on the ground");
const groundedBehavior1 = new ActionWait("wait", 1);
const groundedBehavior2 = new ActionLog("still on the ground");
const groundedBehaviorSequence = new LatchedSequence("sequence", [groundedBehavior0, groundedBehavior1, groundedBehavior2]);

// state - air
const airborneBehavior0 = new ActionLog("in the air");
const airborneBehavior1 = new ActionWait("wait", 1);
const airborneBehavior2 = new ActionLog("still on the air");
const airborneBehaviorSequence = new LatchedSequence("sequence", [airborneBehavior0, airborneBehavior1, airborneBehavior2]);

// CREATE DEFINITION, MACHINE

const model = new State("model");
const characterStates = new Region("region", model);
const initial = new PseudoState("initial", characterStates, PseudoStateKind.Initial);
const stateGround = new CharacterState("grounded", groundedBehaviorSequence, characterStates);
const stateAirborne = new CharacterState("airborne", airborneBehaviorSequence, characterStates);

initial.to(stateGround);
stateGround.on(UpdateCharacterMachineEvent).when(updateCharacter$ => updateCharacter$._character.blackboard.airborne).to(stateAirborne);
stateAirborne.on(UpdateCharacterMachineEvent).when(updateCharacter$ => !updateCharacter$._character.blackboard.airborne).to(stateGround);

// CREATE CHARACTER

log.add(message => console.info(message), log.Entry | log.Exit);

const instance = new Instance("character", model);

// character data 
const character: Character = new Character(instance, characterStates, new CharacterBlackboard());

// make `process.stdin` begin emitting "keypress" events
process.stdin.setRawMode(true);
process.stdin.resume();
process.stdin.on(
  'data',
  (key) => {
    if (key) {
      character.blackboard.airborne = true;
    }
  }
);

// start the update loop
updateLoop();