import { Instance, Region, State } from "@steelbreeze/state";
import { CharacterBlackboard } from "./CharacterBlackboard";

export class Character {

  //#region 

  //#endregion

  //#region Fields

  private _region: Region;

  private _instance: Instance;

  private _blackboard: CharacterBlackboard;

  //#endregion

  //#region Properties

  public get blackboard(): CharacterBlackboard {
    return this._blackboard;
  }

  public get instance(): Instance {
    return this._instance;
  }

  public get region(): Region {
    return this._region;
  }

  //#endregion

  constructor(instance: Instance, regionCharacterStates: Region, blackboard: CharacterBlackboard) {
    this._region = regionCharacterStates;

    this._instance = instance;

    this._blackboard = blackboard;
  }
}